# Module 1 - IIOT Basics


## What is IOT ?
Internet of Things (IoT) is an ecosystem of connected physical objects that are accessible through the internet. 

## What is IIOT ? 
The industrial internet of things (IIoT) refers to the extension and use of the internet of things (IoT) in industrial sectors and applications.

## Industrial Revolutions 
As the name itself suggests an Industrial Revolution is the one in which there is a complete change in the ways the industries work.Currently Industry 3.5 is going on that is we are moving from partial automation to complete automation.

![](https://miro.medium.com/max/2626/1*H9TRBJYWzYEYC1G4wRvwUA.jpeg)

## Industry 3.0 vs Industry 4.0 
In **Industry 3.0**, we automate processes using logic processors and information technology. These processes often operate largely without human interference, but there is still a human aspect behind it.
*For an example of the old way (Industry 3.0), take a CNC machine: while largely automated, it still needs input from a human controller. The process is automated based on human input, not by data.* Hence, here the data is stored in excel sheets and databases.

**Industry 4.0** means lots of data can be collected through sensors and used to make decisions about repairs and upkeep.*For eg - Under Industry 4.0, that same CNC machine would not only be able to follow set programming parameters, but also use data to streamline production processes.*

![](assignments/summary/Comparison-between-characteristics-of-Industry-30-and-Industry-40-in-manufacturing.png)

## Architecture : 

### Industry 3.0 

Sensors installed at various points in the factory send data to PLC's which collect all the data and send it to SCADA and ERP systems for storing the data in excel.

### Industry 4.0 
Data data goes from Controller to Cloud via the Industry 4.0 protocols. Also here every device can communicate with each other via the cloud.

In the given figure left image depicts Industry 3.0 Architecture while right image depicts Industry 4.0 Architecture.

![](assignments/summary/Comparison-of-the-5-layer-architecture-and-CPS-based-automation-according-to-Lueth-2015.png)
 

## Protocols 

### Industry 3.0 

All these protocols are optimized for sending data to a central server inside the factory.

![](assignments/summary/4.png) 


### Industry 4.0 

All these protocols are optimized for sending data to cloud for data analysis.

![](assignments/summary/Capture.JPG)


## Problems with Industry 4 Upgrades

![](assignments/summary/Capture2.JPG)

## Solution

Get data from Industry 3.0 devices/meters/sensors without changes to the original device.
And then send the data to the Cloud using Industry 4.0 devices

## Challanges in conversion from Indstry 3.0 to 4.0 Protocols

![](assignments/summary/Capture3.JPG)

## Making your own IIOT product

![](assignments/summary/4.JPG)

## IOT TSDB Tools

For Storing Data

![](assignments/summary/Capture4.JPG)

## IOT Dashboards

For viewing Data

![](assignments/summary/capture5.JPG)

## IOT Platforms

For analysing data

![](assignments/summary/Capture6.JPG)

## Getting Alerts

For getting alerts based on Data use Zaiper & Twilio.



## How it looks inside the cloud?

![](assignments/summary/Capture9.JPG)





